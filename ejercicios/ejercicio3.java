/**
 *  Programa que, mediante un JFileChooser, añada al fichero el contenido del texto escrito por el usuario en un 
 *  recuadro de texto (JTextField). El programa utilizará un botón que lanzará el selector del fichero y un otro
 *  que escriba a fichero el contenido indicado, después de haberlo seleccionado.
 
    @author Adrián De la Rosa Vicente

*/
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.io.*;

public class ejercicio3 extends JFrame implements ActionListener{
    private File f;
    private JLabel label1;
    private JButton button1;
    private JButton button2;
    private JTextField field1;
    private JFileChooser fileChooser1;
    private String content;

    public ejercicio3(){
        f = null;
        setLayout(null);

        fileChooser1 = new JFileChooser();

        label1 = new JLabel("Contenido");
        label1.setBounds(170,25,100,30);
        add(label1);

        field1 = new JTextField();
        field1.setBounds(100,50,220,30);
        add(field1);
        field1.addActionListener(this);

        button1 = new JButton("Abrir fichero");
        button1.setBounds(40,100,160,30);
        add(button1);
        button1.addActionListener(this);

        button2 = new JButton("Guardar cambios");
        button2.setBounds(220,100,160,30);
        add(button2);
        button2.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e){
        if(e.getSource() == button1){
            f = selectArchivo();
        }
        if(e.getSource() == button2){
            guardarArchivo(f);
        }
    }

    private File selectArchivo(){
        fileChooser1.showOpenDialog(this);
        f = fileChooser1.getSelectedFile();
        return f;
    }

    private void guardarArchivo(File f){

        try{
            if(f != null){
                FileWriter save = new FileWriter(f, true);
                save.write(field1.getText());
                save.close();

                JOptionPane.showMessageDialog(this, "El archivo se ha guardado correctamente");

            }else{
                System.err.println("Referencia nula al fichero seleccionado");
            }
        }catch(IOException e1){
            JOptionPane.showMessageDialog(this, "El archivo no se ha guardado correctamente");
        }
    }

    public static void main(String args[]){
        ejercicio3 ej3 = new ejercicio3();
        ej3.setBounds(0,0,420,200);
        ej3.setLocationRelativeTo(null);
        ej3.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        ej3.setVisible(true);
    }
}