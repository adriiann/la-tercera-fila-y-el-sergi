/**
 *  Realitza una aplicació que genere un número aleatori en fer clic sobre un botó. El nombre de dígits que tindrà l'aleatori el prendrà 
    d'un requadre de text (JTextField). Si el requadre té el valor 3, per exemple, generarà aleatoris entre 0 i 999. Mostrarà 
    el valor generat en una etiqueta.

    @author Adrián De la Rosa Vicente

*/
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import java.io.*;
    
public class ejercicio2 extends JFrame implements ActionListener{
    private JButton button1;
    private JTextField field1;
    private JLabel label1;
    private String content;

    public ejercicio2(){
        setLayout(null);
        
        field1 = new JTextField();
        field1.setBounds(100,50,130,30);
        add(field1);
        field1.addActionListener(this);

        label1 = new JLabel("Aleatorio");
        label1.setBounds(250,50,100,30);
        add(label1);

        button1 = new JButton("Generar");
        button1.setBounds(160,100,100,30);
        add(button1);
        button1.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e){
        int numeroAzar = 0, valor = 0, potencia;
        if(e.getSource() == button1){
            valor = Integer.parseInt(field1.getText());
            potencia = (int)Math.pow(10, valor);
            numeroAzar = (int)(Math.random() * potencia);
            label1.setText(String.valueOf(numeroAzar));
        }
        
        try(FileWriter fw = new FileWriter("registroSorteo.txt", true)){                
            fw.write(String.valueOf(numeroAzar) + "\n");
        }catch(IOException e1){
            System.err.println(e1.getMessage());
        }
    }

    public static void main(String args[]){
        ejercicio2 ej2 = new ejercicio2();
        ej2.setBounds(0,0,420,200);
        ej2.setLocationRelativeTo(null);
        ej2.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        ej2.setVisible(true);
    }
}