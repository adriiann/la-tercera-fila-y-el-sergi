/**
 *  Realiza una apliación gráfica que permita a un bibliotecario listar las películas
    guardados al fichero ("films.dat") generado para el ejercicio 11 de la unidad anterior
    de entrada/salida. Para mostrar los datos de cada película tienes que utilizar componentes
    JTextField, de manera que con cada click sobre un botón llamado "Siguiente" irá mostrandoo los 
    valores para cada película.

    @author Adrián De la Rosa Vicente
    
*/

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.io.*;
import java.util.ArrayList;

public class ejercicio5 extends JFrame implements ActionListener{

    private JLabel label1;
    private JTextField textfield1;
    private JButton button1;
    private int indexPelicula;
    private ArrayList<String> listadoPelicula;

    public ejercicio5(){
        setLayout(null);
        setTitle("Ejercicio 5");

        label1 = new JLabel("Películas disponibles");
        label1.setBounds(130,15,200,30);
        add(label1);

        textfield1 = new JTextField();
        textfield1.setBounds(110,60,200,30);
        add(textfield1);

        button1 = new JButton("Siguiente >>");
        button1.setBounds(130,105,160,30);
        add(button1);
        button1.addActionListener(this);
    }

    /*private ArrayList<String> leerPeliculas(String archivo){
        try(BufferedReader br = new BufferedReader(new FileReader(archivo))){
            String linea;
            while((linea = br.readLine()) != null){
                listadoPelicula.add(linea);
            }
        }catch(IOException e1){
            System.err.println(e1.getMessage());
        }

        return listadoPelicula;
    }*/

    /*private void mostrarSiguiente(){
        if(indexPelicula < listadoPelicula.size()){
            textfield1.setText(listadoPelicula.get(indexPelicula));
            indexPelicula++;
        }else{
            JOptionPane.showMessageDialog(this, "No hay más películas disponibles");
        }
    }*/

    public void actionPerformed(ActionEvent e){
        if(button1 == e.getSource()){
            //leerPeliculas("films.txt");
            //mostrarSiguiente();
        }
    }

    public static void main(String args[]){
        ejercicio5 ej5 = new ejercicio5();
        ej5.setBounds(0,0,420,200);
        ej5.setLocationRelativeTo(null);
        ej5.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        ej5.setVisible(true);
    }
}   