/**
 *  Programa que, mitjançant 3 botons ( 0 , 1 i C per a esborrar) permeta compondre un valor numèric en binari 
    que serà mostrat en binari i en decimal amb les corresponents etiquetes. 


    @author Adrián De la Rosa Vicente
*/

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class ejercicio1 extends JFrame implements ActionListener{
    private JButton button1, button2, button3;
    private JLabel binario, decimal;
    private String content = "";
    private int cont = 0;

    public ejercicio1(){
        setLayout(null);

        binario = new JLabel("Binario");
        binario.setBounds(120, 50, 100, 30);
        add(binario);

        decimal = new JLabel("Decimal");
        decimal.setBounds(240, 50, 100, 30);
        add(decimal);

        button1 = new JButton("0");
        button1.setBounds(50,100,90,30);
        add(button1);
        button1.addActionListener(this);

        button2 = new JButton("1");
        button2.setBounds(170,100,90,30);
        add(button2);
        button2.addActionListener(this);

        button3 = new JButton("C");
        button3.setBounds(280,100,90,30);
        add(button3);
        button3.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e){

        if(cont < 8) {
            if(e.getSource() == button1){
                content+=0;
                binario.setText(content);
            }
            if(e.getSource() == button2){
                content+=1;
                binario.setText(content);
            }
            cont++;
        }

        if(content != ""){
            int valorDecimal = Integer.parseInt(content, 2);
            decimal.setText(String.valueOf(valorDecimal));
        }else{
            decimal.setText("Decimal");
        }

        if(e.getSource() == button3){
            content = "";
            binario.setText("Binario");
            decimal.setText("Decimal");
            cont = 0;
        }
    }
    
    public static void main(String args[]){
        ejercicio1 ej1 = new ejercicio1();
        ej1.setBounds(0,0,420,200);
        ej1.setLocationRelativeTo(null);
        ej1.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        ej1.setVisible(true);
    }

}