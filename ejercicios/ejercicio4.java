/**
 *  Programa que utilizando 3 componentes JSlider con valores de 0 a 255, permita seleccionar graduacion de color
    para los 3 colores RGB (rojo, verde y azul). El programa mostrará otro componentes (etiqueta o recuadro de texto) el
    color resultante para los 3 valores seleccionados en la graduacion. También puedes añadir 3 componentes CheckBox que
    permitan seleccionar o deseleccionar cada color por separado, es decir si el checkbox no tiene un color seleccionado 
    actuará como si el slider estuviera a 0
    
    @author Adrian De la Rosa Vicente
*/

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class ejercicio4 extends JFrame{

    private JTextField field1;
    private JLabel red;
    private JSlider slider1;
    private JLabel green;
    private JSlider slider2;
    private JLabel blue;
    private JSlider slider3;
    private int valueRed, valueGreen, valueBlue;


    public ejercicio4(){
        setTitle("Ejercicio 4");
        setLayout(null);

        field1 = new JTextField();
        field1.setBounds(115,100,200,50);
        add(field1);


        red = new JLabel("Red --> ");
        slider1 = new JSlider(JSlider.HORIZONTAL, 0,255,128);
        slider1.addChangeListener(e -> movSlider(e));
        red.setBounds(50,15,70,30);
        slider1.setBounds(115, 15, 200, 30);
        add(slider1);
        add(red);

        green = new JLabel("Green --> ");
        slider2 = new JSlider(JSlider.HORIZONTAL, 0,255,128);
        slider2.addChangeListener(e -> movSlider(e));
        green.setBounds(40,40,80,30);
        slider2.setBounds(115,40,200,30);
        add(slider2);
        add(green);

        blue = new JLabel("Blue --> ");
        slider3 = new JSlider(JSlider.HORIZONTAL, 0,255,128);
        slider3.addChangeListener(e -> movSlider(e));
        blue.setBounds(50,65,70,30);
        slider3.setBounds(115,65,200,30);
        add(slider3);
        add(blue);

        pack();
    }


    public void movSlider(ChangeEvent e){
        JSlider obj = (JSlider)e.getSource();
        if(!obj.getValueIsAdjusting()){
            valueRed = slider1.getValue();
            valueGreen = slider2.getValue();
            valueBlue = slider3.getValue();
            field1.setBackground(new Color(valueRed,valueGreen,valueBlue));
        }
    }

    public static void main(String args[]){
        ejercicio4 ej4 = new ejercicio4();
        ej4.setBounds(0,0,420,200);
        ej4.setLocationRelativeTo(null);
        ej4.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        ej4.setVisible(true);
    }
}
