/**
 *  Modifica el ejercicio1.java para usar solo una etiqueta que muestre el valor en binario o decimal segun la opción 
 *  escogida en el menú (un JMenu con 2 JMenuItems (binario y decimal))
 * 
 *  @author Adrián De la Rosa Vicente
*/

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ejercicio7 extends JFrame implements ActionListener{
    private JMenu jm;
    private JMenuBar jmb;
    private JMenuItem binarioItem;
    private JMenuItem decimalItem;
    private JTextField result;
    private int cont = 0;

    public ejercicio7(){
        setTitle("CONVERSIÓN BINARIO-DECIMAL");
        setLayout(null);

        jmb = new JMenuBar();
        setJMenuBar(jmb);

        jm = new JMenu("Operaciones disponibles");
        jmb.add(jm);

        binarioItem = new JMenuItem("Binario");
        jm.add(binarioItem);
        binarioItem.addActionListener(this);
    
        decimalItem = new JMenuItem("Decimal");
        jm.add(decimalItem);
        decimalItem.addActionListener(this);
    
        result = new JTextField();
        result.setBounds(140,50,100,30);
        add(result);
    }

    public void actionPerformed(ActionEvent e){
        if((binarioItem == e.getSource())){
            int decimal = Integer.parseInt(result.getText());
            String binario = Integer.toBinaryString(decimal);
            result.setText(binario);
        }

        if((decimalItem == e.getSource())){
            int valorDecimal = Integer.parseInt(result.getText(), 2);
            result.setText(String.valueOf(valorDecimal));
        }
    }

    public static void main(String args[]){
        ejercicio7 ej7 = new ejercicio7();
        ej7.setBounds(0,0,400,250);
        ej7.setLocationRelativeTo(null);
        ej7.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        ej7.setVisible(true);
    }

}