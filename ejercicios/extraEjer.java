/**
 *  Programa que el usuario tenga que introducir dos números y existen 4 botones (suma, resta, multiplicación y división), se
 *  introducira el resultado de la operación en un componente JLabel.
 * 
*/

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class extraEjer extends JFrame implements ActionListener{
    private JTextField jfield1;
    private JTextField jfield2;
    private JButton jbutton1;
    private JButton jbutton2;
    private JButton jbutton3;
    private JButton jbutton4;
    private JLabel label1;

    public extraEjer(){
        setTitle("Operaciones aritméticas");
        setLayout(null);

        jbutton1 = new JButton("Suma");
        jbutton1.setBounds(55,25,100,30);
        add(jbutton1);
        jbutton1.addActionListener(this);

        jbutton2 = new JButton("Resta");
        jbutton2.setBounds(55,65,100,30);
        add(jbutton2);
        jbutton2.addActionListener(this);

        jbutton3 = new JButton("Multiplicación");
        jbutton3.setBounds(55,105,135,30);
        add(jbutton3);
        jbutton3.addActionListener(this);

        jbutton4 = new JButton("División");
        jbutton4.setBounds(55,145,135,30);
        add(jbutton4);
        jbutton4.addActionListener(this);

        jfield1 = new JTextField();
        jfield1.setBounds(300,60,55,30);
        add(jfield1);

        jfield2 = new JTextField();
        jfield2.setBounds(300,100,55,30);
        add(jfield2);

        label1 = new JLabel("Resultado --> ");
        label1.setBounds(140,200,200,30);
        add(label1);
    }

    public void actionPerformed(ActionEvent e){
        
        if(jbutton1 == e.getSource()){
            double numero1 = Double.parseDouble(jfield1.getText());
            double numero2 = Double.parseDouble(jfield2.getText());

            double resultado = numero1 + numero2;

            label1.setText(String.valueOf("Resultado --> " + resultado));
        }

        if(jbutton2 == e.getSource()){
            double numero1 = Double.parseDouble(jfield1.getText());
            double numero2 = Double.parseDouble(jfield2.getText());

            double resultado = numero1 - numero2;

            label1.setText(String.valueOf("Resultado --> " + resultado));
        }

        if(jbutton3 == e.getSource()){
            double numero1 = Double.parseDouble(jfield1.getText());
            double numero2 = Double.parseDouble(jfield2.getText());

            double resultado = numero1 * numero2;

            label1.setText(String.valueOf("Resultado --> " + resultado));
        }

        if(jbutton4 == e.getSource()){
            double numero1 = Double.parseDouble(jfield1.getText());
            double numero2 = Double.parseDouble(jfield2.getText());

            double resultado = numero1 / numero2;

            label1.setText(String.valueOf("Resultado --> " + resultado));
        }

    }

    public static void main(String args[]){
        extraEjer extra = new extraEjer();
        extra.setBounds(0,0,420,300);
        extra.setLocationRelativeTo(null);
        extra.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        extra.setVisible(true);
    }
}