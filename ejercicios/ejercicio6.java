/**
 *  Realitza el següent programa que permeta generar un nombre aleatori entre un mínim i un màxim introduïts per l'usuari en dos requadres de text. El número generat es mostrarà mitjançant una etiqueta en fer 
 *  clic sobre el botó. Observa la captura de l'aplicació a "exercici6.jpg".
  
    @author Adrián De la Rosa Vicente
*/

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

public class ejercicio6 extends JFrame implements ActionListener{
    private JLabel labelMin;
    private JTextField valorMin;
    private JLabel labelMax;
    private JTextField valorMax;
    private JButton generarButton;
    private JLabel numAleatorio;


    public ejercicio6(){
        setTitle("GENERADOR DE NÚMERO ALEATORIO");
        setLayout(null);

        labelMin = new JLabel("Mínimo: ");
        labelMin.setBounds(80,50,80,30);
        add(labelMin);

        valorMin = new JTextField();
        valorMin.setBounds(145,50,50,30);
        add(valorMin);

        labelMax = new JLabel("Máximo: ");
        labelMax.setBounds(250,50,80,30);
        add(labelMax);

        valorMax = new JTextField();
        valorMax.setBounds(315,50,50,30);
        add(valorMax);
    
        generarButton = new JButton("Generar");
        generarButton.setBounds(110,100,100,30);
        add(generarButton);
        generarButton.addActionListener(this);
    
        numAleatorio = new JLabel("Nº aleatorio --> ");
        numAleatorio.setBounds(250,100,150,30);
        add(numAleatorio);
    }

    private int generarAleatorio(int min, int max){
        int aleatorio = (int)(Math.random() * (max - min + 1)) + min;
        return aleatorio;

    }

    public void actionPerformed(ActionEvent e){
        if(generarButton == e.getSource()){
            try{
                int min = Integer.parseInt(valorMin.getText());
                int max = Integer.parseInt(valorMax.getText());

                int numeroAleatorio = generarAleatorio(min, max);

                numAleatorio.setText("Nº aleatorio --> " + Integer.toString(numeroAleatorio));
            }catch(NumberFormatException e1){
                JOptionPane.showMessageDialog(this, "Introduce números válidos");
            }
        }
    }

    public static void main(String args[]){
        ejercicio6 ej6 = new ejercicio6();
        ej6.setBounds(0,0,450,200);
        ej6.setLocationRelativeTo(null);
        ej6.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        ej6.setVisible(true);
    }
}